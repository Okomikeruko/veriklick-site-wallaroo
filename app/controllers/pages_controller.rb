class PagesController < ApplicationController
  
  def home 
  end
  
  def veriklick
  end
  
  def veridial
  end
  
  def verisource
  end
  
  def pricing
  end
  
  def demo 
  end
  
  def get_started 
  end 
  
  def contact 
  end
  
  def support
  end
  
  def about 
  end
  
  def careers 
  end
  
  def registration
  end
  
  def blog 
  end
  
  def article_one 
  end 
  
  def article_two
  end
  
  def article_three
  end
  
  def article_four
  end
  
  def article_five
  end
  
  def article_six
  end
  
  def login 
  end
  
  def signup 
  end
  
  def video 
  end
  
end
