Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  root "pages#home"
  
  get "products/veriklick", to: "pages#veriklick"
  get "products/veridial", to: "pages#veridial"
  get "products/verisource", to: "pages#verisource"
  get "pricing", to: "pages#pricing"
  get "demo", to: "pages#demo"
  get "get-started", to: "pages#get_started"
  get "contact", to: "pages#contact"
  get "support", to: "pages#support"
  get "about", to: "pages#about"
  get "careers", to: "pages#careers"
  get "registration", to: "pages#registration"
  get "blog", to: "pages#blog"
  get "blog/winning-the-war-for-talent-it-all-depends-on-ai", to: "pages#article_one", as: "article_one"
  get "blog/verification-with-a-klick-hire-right-the-first-time", to: "pages#article_two", as: "article_two"
  get "blog/best-recruiting-practices-series-answering-your-questions", to: "pages#article_three", as: "article_three"
  get "blog/best-recruiting-practices-2018", to: "pages#article_four", as: "article_four"
  get "blog/synkriom-the-onestop-shop-for-verified-resources", to: "pages#article_five", as: "article_five"
  get "blog/new-tech-aims-to-id-deceptive-candidates", to: "pages#article_six", as: "article_six"
  get "login", to: "pages#login"
  get "signup", to: "pages#signup"
  
  get "video", to: "pages#video"
  
end
